# Introduction
- What are containers(intermodal)? Why are they so useful?
- Distribution and Operation (Shipment Dependencies)

## Containers and images
- Definitions: Process, Program, Binary...
- Copy a binary from system files experiment.
    - What is the problem ?
    - Who can we solve it ?
    - Either Give you my machine or give you instructions.
    - The versioning problem ?
- Make a Docker image with ubuntu and stuff
- Image CRUD Operations

## Containers
- What is a Linux Process? Containing a process?
- What do we need to run a process: Kernel, Sys libs, binary...
- Containers = ZIP + Kernel features!

# Containers
### CRUD Operations
- Create -> `docker create` or `docker run`
- Read -> `docker ps`
- Update -> `docker exec` (Not really)
- Delete -> `docker rm`

# Images
### CRUD Operations
- Create -> `docker build`
- Read -> `docker image ls`
- Update -> `docker build`
- Delete -> `docker rmi`

# Volumes
### CRUD Operations
- Create -> `docker volume create`
- Read -> `docker volume ls`
- Update -> None
- Delete -> `docker volume rm`

## Networks


## Cool Things

## Resources:
- [Katakoda](https://www.katakoda.com)
