from time import sleep
import threading as td

def cook_chicken():
    print("1.cooking chicken")
    sleep(2.5)

def prepare_dough():
    print("2.prepare dough")
    sleep(0.5)

def prepare_stuffing():
    print("3.prepare stuffing")
    sleep(2)

def stuff_pastilla():
    print("4.stuffing pastilla")
    sleep(1)

def make_pastilla():
    cook_chicken()
    prepare_dough()
    prepare_stuffing()
    stuff_pastilla()

if __name__ == "__main__":
    print("pastilla 1")
    t1 = td.Thread(target=make_pastilla)

    print("\npastilla 2")
    t2 = td.Thread(target=make_pastilla)

    t1.start()
    t2.start()

    t1.join()
    t2.join()

    print("\ndone")
