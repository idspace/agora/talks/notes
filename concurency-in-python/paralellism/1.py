from time import sleep
import multiprocessing as mp

def cook_chicken():
    print("1.cooking chicken")
    sleep(2.5)

def prepare_dough():
    print("2.prepare dough")
    sleep(0.5)

def prepare_stuffing():
    print("3.prepare stuffing")
    sleep(2)

def stuff_pastilla():
    print("4.stuffing pastilla")
    sleep(1)

def make_pastilla():
    cook_chicken()
    prepare_dough()
    prepare_stuffing()
    stuff_pastilla()

if __name__ == "__main__":
    print("Number of processors: ", mp.cpu_count())

    print("pastilla 1")
    p1 = mp.Process(target=make_pastilla)

    print("\npastilla 2")
    p2 = mp.Process(target=make_pastilla)

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    print("\ndone")
