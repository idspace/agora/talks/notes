from time import sleep
import sys
import multiprocessing as mp

def cook_chicken():
    print("1.cooking chicken")
    sleep(2.5)

def prepare_dough():
    print("2.preparing dough")
    sleep(0.5)

def prepare_stuffing():
    print("3.preparing stuffing")
    sleep(2)

def stuff_pastilla():
    print("4.stuffing pastilla")
    sleep(1)

def Prepare_pastilla(conn):
    cook_chicken()
    prepare_dough()
    conn.send("xstuffing")
    conn.close()

def Stuff_pastilla(conn):
    if (conn.recv() != "xstuffing"):
        sys.exit(1)
    prepare_stuffing()
    stuff_pastilla()

if __name__ == "__main__":
    print("Number of processors: ", mp.cpu_count())

    first_end, last_end = mp.Pipe()


    print("Prepare Pastilla")
    p1 = mp.Process(target=Prepare_pastilla, args=(first_end,))
    print("\nStuff")
    p2 = mp.Process(target=Stuff_pastilla, args=(last_end,))

    p1.start()
    p2.start()

    p1.join()
    p2.join()

    print(p1.exitcode)
    print(p2.exitcode)

    print("\ndone")
