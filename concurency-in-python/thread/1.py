#!/usr/bin/env python3

import asyncio

async def cook_chicken():
    print("1.cooking chicken")
    await asyncio.sleep(2.5)

async def prepare_dough():
    print("2.prepare dough")
    await asyncio.sleep(0.5)

async def prepare_stuffing():
    print("3.prepare stuffing")
    await asyncio.sleep(2)

async def stuff_pastilla():
    print("4.stuffing pastilla")
    await asyncio.sleep(1)

async def main():
    await asyncio.gather(cook_chicken(), prepare_dough(), prepare_stuffing(), stuff_pastilla())

if __name__ == "__main__":
    print("Making pastilla")
    asyncio.run(main())
    print("\ndone")
