# Introduction to containers

This talk serves as an introduction to linux containers. From the history behind containers to the technologies behind Linux containers.

| title | slides | event | video |
| ---   | ---    | ---   | ---   |
| https://linuxcontainers.org/lxd/docs/master/ | [https://slides.iduoad.com/intro-containers/#/](https://slides.iduoad.com/intro-containers/#/) | [SUSE Cloud Native Foundations Scholarship Program](https://www.udacity.com/scholarships/suse-cloud-native-foundations-scholarship) | [Youtube](https://www.youtube.com/watch?v=42_cZZAdWjc) (Darija*)

### Useful links
- [A Brief History of Containers: From the 1970s Till Now - Youtube](https://blog.aquasec.com/a-brief-history-of-containers-from-1970s-chroot-to-docker-2016)
- [Surge 2015 - Bryan Cantrill - Docker in Production: Tales From the Engine Room](https://www.youtube.com/watch?v=0T2XFSALOaU)**
- [The Container Revolution: Reflections After the First Decade - Youtube](https://www.youtube.com/watch?v=xXWaECk9XqM)
- [Containers From Scratch • Liz Rice • GOTO 2018 - Youtube](https://www.youtube.com/watch?v=8fi7uSYlOdc)
- [Namespaces - manpage](https://man7.org/linux/man-pages/man7/namespaces.7.html)
- [Unshare command - manpage](https://man7.org/linux/man-pages/man1/unshare.1.html)
- [Unshare syscall - manpage](https://man7.org/linux/man-pages/man2/unshare.2.html)
- [Cgroups - manpage](https://man7.org/linux/man-pages/man7/cgroups.7.html)
- [Resource Management Guide - redhat docs](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/resource_management_guide/index)
- [Cgroups - Arch wiki](https://wiki.archlinux.org/title/cgroups)
- [Capabilities - manpage](https://man7.org/linux/man-pages/man7/capabilities.7.html)
- [Capsh - manpage](https://man7.org/linux/man-pages/man1/capsh.1.html)
- [https://blog.container-solutions.com/linux-capabilities-in-practice](https://blog.container-solutions.com/linux-capabilities-in-practice)
- [Using SELinux with container runtimes - Youtube](https://www.youtube.com/watch?v=FOny29a31ls)***
- [Hardening Docker with SELinux on CentOS 8 - Youtube](https://www.youtube.com/watch?v=sT9Q2xZr4nk)
- [Seccomp Security Profiles and You: A Practical Guide - Duffie Cooley, VMware](https://www.youtube.com/watch?v=OPuu8wsu2Zc)
- [Docker seccomp profile - Github](https://github.com/moby/moby/blob/master/profiles/seccomp/default.json)
- [Docker documentaion](https://docs.docker.com/)
- [Podman Blog](https://podman.io/blogs/)
- [Buildah Blog](https://buildah.io/blogs/)
- [Managing Containers](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/managing_containers/index)
- [LXD documentation](https://linuxcontainers.org/lxd/docs/master/)

### Snippets
```shell
# PID namespaces
sudo unshare --fork --pid --mount-proc bash

# Mount namespaces
unshare -m /bin/bash
secret_dir=`mktemp -d --tmpdir=/tmp`
echo $secret_dir
mount -n -o size=1m -t tmpfs tmpfs $secret_dir
df -hT

# UTS namespaces
sudo unshare --uts bash

# User namespaces
sudo unshare --user --map-root-user bash

# Network namespaces
sudo unshare --net bash

# Capabilities
cat /proc/<pid>/status
```

### Notes
- (*): Darija is the Arabic Dialect used in Morocco 🇲🇦
- (**): [Bryan](https://twitter.com/bcantrill) talks about containers and technology are always the best.
- (***): [Dan Walsh](https://twitter.com/rhatdan) aka [Mr SELinux](https://danwalsh.livejournal.com/) is one of the best engineers at Redhat and one of the most important contributors to Linux and containers.
